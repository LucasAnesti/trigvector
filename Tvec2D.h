/** @file Tvec2D.h 
	@author: Lucas Anesti  */
/*	Description: A class that represents a vector on the x,y 
	coordinate system  */

#ifndef TVEC2DH
#define TVEC2DH

#include <iostream>
#include <cmath>		// sin, cos, atan, fmod
#include <stdexcept>	// std::logic_error
#include <memory> 		// std::shared_ptr
#include "TvecInterface.h"
#include "Point2D.h"

/* 	Class Invariant:
		Tvec2D stores represent a vector object by storing
		the terminal point of the vector. Its magnitude is
		calculated by calculating its distance from the
		origin.	*/
template<class DataType>
class Tvec2D: public TvecInterface<DataType>
{
private:

	// The terminal point of the vector
	Point2D<DataType> termPoint;

public:

	// Constructors

	/** Default constructor.
	@post The terminal point is set to (0,0)	*/
	Tvec2D();

	/** Custom constructor that accepts a point.
	@param p  A Point2D object.
	@post The terminal point is set to the given point */
	Tvec2D(const Point2D<DataType>& p);

	/** Custom constructor that accepts two points.
	@param p1 A Point2D object that represents the base point of the vector.
	@param p2 A Point2D object that represents the terminal point of the
		vector.
	@post Using the Head Minus Tail rule, the corresponding x and y 
		coordinates are subtracted to translate the vector to the
		standard position with base at (0,0). */
	Tvec2D(const Point2D<DataType>& p1,const Point2D<DataType>& p2); 

	/** Custom constructor that accepts a magnitude and a direction. 
	@param mag  The desired magnitude of the vector.
	@param angle  The desired angle of the vector.
	@post The values of mag and angle are converted into a terminal 
		point that represents a standard vector.  */
	Tvec2D(const DataType& mag, const radians& angle);



	// Getters

	/** Get the x coordinate of the terminal point of the vector.
	@return the x coordinate. */
	DataType x() const;

	/** Get the y coordinate of the terminal point of the vector.
	@return the y coordinate. */
	DataType y() const;



	// Setters

	/** Change the x coordinate of the terminal point
	@param newX The new x value of the terminal point
	@param newY The new y value of the terminal point.
	@post  The x and yvalue of the terminal point are set to
		newX and newY.	*/
	void set(const DataType& newX, const DataType& newY);




	// Methods from TvecInterface

	DataType magnitude() const;

	radians direction() const;

  	std::shared_ptr< TvecInterface<DataType> > unit() const;

	DataType dot(
		const std::shared_ptr< TvecInterface<DataType> >& other) const;

	radians angle(
		const std::shared_ptr< TvecInterface<DataType> >& other) const;

	bool isEqual(
		const std::shared_ptr< TvecInterface<DataType> >& other) const;

	bool isOrtho(
		const std::shared_ptr< TvecInterface<DataType> >& other) const;

 	std::shared_ptr< TvecInterface<DataType> > 
		add( const std::shared_ptr< TvecInterface<DataType> >& other) const;

	std::shared_ptr< TvecInterface<DataType> > 
		mul( const DataType& scalar) const;

 	std::shared_ptr<TvecInterface<DataType>> 
		sub( const std::shared_ptr< TvecInterface<DataType> >& other) const;

	std::shared_ptr< TvecInterface<DataType> > 
		div( const DataType& scalar) const;


	std::ostream& print(std::ostream& os) const; 
	
};




#include"Tvec2D.cpp"


#endif
