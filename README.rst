====
Tvec
====
* (T)rigonometric (Vec)tors. A menu driven program to perform standard trig operations on vectors.
* Author: Lucas Anesti
* Date: Dec 26, 2018

Purpose:
--------
Exercise. Primary learning objectives include exploration of the Object Oriented Programming paradigm and application of abstract classes, inheritance, polymorphism, object composition, and templates to convepts in trigonometry.


System and Tools:
-----------------
* Linux Mint 17.3 Cinnamon 64-bit
* GNU Make 3.81
* g++ 4.8.4
* C++11
* git version 1.9.1

To use:
-------
* **make**: Compile source files and generate an executable.
* **make run**: Run the program.
* **make test**: Run the driver program test.cpp that tests class methods.
* **make clean**: Remove the executable and other project files.
