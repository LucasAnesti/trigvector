/** @file Tvec2D.cpp
 	@author: Lucas Anesti */
/*	Description: The implementation file for the Tvec2D class  */

#ifndef TVEC2DCPP
#define TVEC2DCPP

#include"Tvec2D.h"


// Constructors

template <class DataType>
Tvec2D<DataType>::Tvec2D(): termPoint(0,0){}

template <class DataType>
Tvec2D<DataType>::Tvec2D(const Point2D<DataType>& point)
						:termPoint(point.getx(), point.gety()){}

template <class DataType>
Tvec2D<DataType>::Tvec2D(const Point2D<DataType>& p1,
						 const Point2D<DataType>& p2)
{
	termPoint.set( p2.getx()-p1.getx(), p2.gety()-p1.gety() );
} 

template <class DataType>
Tvec2D<DataType>::Tvec2D(const DataType& mag, const radians& angle)
{
	termPoint.set( mag * cos(angle), mag * sin(angle) );
}



// Getters

template <class DataType>
DataType Tvec2D<DataType>::x() const
{
	return termPoint.getx();
}

template <class DataType>
DataType Tvec2D<DataType>::y() const
{
	return termPoint.gety();
}



// Setters

template <class DataType>
void Tvec2D<DataType>::set(const DataType& newX,const DataType& newY)
{
	termPoint.set(newX, newY);
}



// Methods from TvecInterface

template <class DataType>
DataType Tvec2D<DataType>::magnitude() const
{
	return termPoint.dis();
}

template <class DataType>
radians Tvec2D<DataType>::direction() const
{
	radians dir;

	// Enforce precondition: Can't have 0 magnitude.
	if(this->magnitude() == 0 )
		throw std::logic_error("Error: mag = 0.");

	// Set flags to indicate the quadrant in which the angle lies.
	bool posx = termPoint.getx() > 0;
	bool posy = termPoint.gety() > 0;

	/* If angle lies in 1st or 4th quadrants can apply arctan, 
	   if not in 1st or 4th quadrant, modify angle to meet the 
	   domain requirements of arctan. */
	if( ! posx )
	{
		// If the point lies on the vertical axis, then
		// it's Pi/2 or 3Pi/2 radians.
		if(termPoint.getx()==0)
		{
			dir = (posy ? Pi/2 : 3*(Pi/2) );
		}
		else
		{
			// Invert the sign of x and y to get the domain correct 
			// for arctan.
			dir = atan( -termPoint.gety() / -termPoint.getx() );
			// Add Pi to restore angle to its original quadrant.
			dir = Pi + dir;
		}
	}
	else
	{
		dir = atan(termPoint.gety()/termPoint.getx());
	}

	// Express the angle as 0 < angle < 2Pi.
	dir = fmod(dir, 2*Pi);
	// To express the angle in positive measure o to 2Pi, 
	// keep adding 2Pi.
	if(dir<0 ){
		dir += 2*Pi;
	}

	return dir;
}


template <class DataType>
std::shared_ptr< TvecInterface<DataType> > Tvec2D<DataType>::
unit() const
{
	DataType m = this->magnitude();
	// Enforce the precondition
	if(m == 0)
	{
		throw("Error: mag = 0");
	}

	return this->mul(1.0 / m);
}

// dot product = vx*ux + vy*uy, 
template <class DataType>
DataType Tvec2D<DataType>::
dot(const std::shared_ptr< TvecInterface<DataType> >& other) const
{
	/* Downcasting is necessary as the base class 
	   TVecInterface has no x and y coordinates.  */
	const std::shared_ptr<Tvec2D<DataType>> temp = 
					std::dynamic_pointer_cast<Tvec2D<DataType>>(other);
	return ( this->x()*temp->x() + this->y()*temp->y() );
}

template <class DataType>
radians Tvec2D<DataType>::
angle( const std::shared_ptr< TvecInterface<DataType> >& other) const
{
	// cos a = (u.dot(v) / |u|*|v|). Solving for 'a' gives the angle.
	// angle is always between 0 and Pi degrees.
	double numer = this->dot(other);
	double denom = this->magnitude() * other->magnitude();
	if( denom == 0)
	{
		throw std::logic_error("Error: mag = 0");
	} 

	return acos( numer / denom );
}

template <class DataType>
bool Tvec2D<DataType>::
isEqual( const std::shared_ptr< TvecInterface<DataType> >& other) const
{
	// Two vectors are equal if their magnitudes and directions are equal.
	// The vector directions can only be calculated for nonzero vectors.
	// A vector is equal to a zero vector only if its magnitude is zero.
	if(this->magnitude() == 0.0 || other->magnitude() == 0.0)
	{
		return  ( this->magnitude() == other->magnitude() );
	}
	else
	{
		return ( this->magnitude() == other->magnitude() )
			&& ( this->direction() == other->direction() );
	}
}

template <class DataType>
bool Tvec2D<DataType>::
isOrtho( const std::shared_ptr< TvecInterface<DataType> >& other) const
{
	// Two vectors are orthogonal if their dot product is 0
	return (this->dot(other))== 0;
}

template <class DataType>
std::shared_ptr<TvecInterface<DataType>> Tvec2D<DataType>::
add( const std::shared_ptr< TvecInterface<DataType> >& vec) const
{
	// Downcasting to perform coordinate additions
	const std::shared_ptr< Tvec2D<DataType> > temp = 
					std::dynamic_pointer_cast< Tvec2D<DataType> >(vec);
	// Add the x's and y's of the two vectors and create a new base 
	// dynamic pointer from the result
	Point2D<DataType> p( this->x()+temp->x(), this->y()+temp->y() );
	std::shared_ptr<TvecInterface<DataType> > resultVector = 
								std::make_shared< Tvec2D<DataType> >(p);

	// Return a polymorhpic base pointer
	return resultVector;
}

template <class DataType>
std::shared_ptr< TvecInterface<DataType> > Tvec2D<DataType>::
mul( const DataType& scalar) const
{
	Point2D<DataType> p( scalar*this->x(), scalar*this->y()) ;
	std::shared_ptr< TvecInterface<DataType> > vec = 
							std::make_shared< Tvec2D<DataType> >(p);
	return vec;
}

template <class DataType>
std::shared_ptr< TvecInterface<DataType> > Tvec2D<DataType>::
sub( const std::shared_ptr< TvecInterface<DataType> >& vec) const
{
	auto temp = vec->mul(-1);
	return this->add(temp);
}



template <class DataType>
std::shared_ptr< TvecInterface<DataType> > Tvec2D<DataType>::
div( const DataType& scalar) const
{
	DataType temp = 0;
	// Enforce the precondition
	if(scalar == 0)
	{
		throw std::logic_error("Error: Div by 0");
	}
	else
	{
		temp = 1/scalar;
	}

	return mul(temp);
}



template <class DataType>
std::ostream& Tvec2D<DataType>::print(std::ostream& os) const
{
	return os << "<" << termPoint.getx()<<"," <<termPoint.gety()<<">";
}




#endif
