/**	@file main.cpp
	@author  Lucas Anesti	*/

#include <cstdlib>	// EXIT_SUCCESS
#include <iostream>
#include <string>

#include "Point2D.h"
#include "Tvec2D.h"
#include "menu.cpp"



int main()
{

	menu();

	return EXIT_SUCCESS;
}
