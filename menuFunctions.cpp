/** @file functions.h 
 	@author: Lucas Anesti */
/*	Description: Functions used by the menu and main driver  */

#ifndef MENUFUNCTIONS
#define MENUFUNCTIONS

#include <iostream>
#include <fstream>
#include <iomanip> 	// std::setw
#include <string>
#include <memory>	// std::shared_ptr
#include <map>		// std::map
#include <iterator> // std::iterator

#include "Point2D.h"
#include "Tvec2D.h"


/** Display the user menu */
void displayMenu()
{

	const int WIDTH = 27;

	std::cout << std::string(WIDTH-1, '-') << " MENU " 
			  << std::string(WIDTH-1,'-')  << std::endl;
	std::cout << std::left 
			  << std::setw(WIDTH) << "L) List of vectors" 
			  << std::setw(WIDTH) << "1) Add two vectors" 
			  << std::endl;
	std::cout << std::left 
			  << std::setw(WIDTH) << "N) New vector" 
			  << std::setw(WIDTH) << "2) Subtract a vector from another" 
			  << std::endl;
	std::cout << std::left 
			  << std::setw(WIDTH) << "D) Delete a vector" 
			  << std::setw(WIDTH) << "3) Multiply a vector with a scalar" 
			  << std::endl;
	std::cout << std::left 
			  << std::setw(WIDTH) << "G) Ground/Air speed" 
			  << std::setw(WIDTH) << "4) Divide a vector by a scalar" 
			  << std::endl;
	std::cout << std::left 
			  << std::setw(WIDTH) << "Q) Save and QUIT" 
			  << std::setw(WIDTH) << "5) Display vector info" 
			  << std::endl;
	std::cout << std::left 
			  << std::setw(WIDTH) << " " 
			  << std::setw(WIDTH) << "6) Dot product of two vectors" 
			  << std::endl;
	std::cout << std::left 
			  << std::setw(WIDTH) << " " 
			  << std::setw(WIDTH) << "7) Unit vector of a vector" 
			  << std::endl;

	std::cout << std::string(WIDTH*2 + 4,'-') << std::endl;
	std::cout << "Please make a selection: ";

}


/** Load the vector list that was saved in the last session
@return  the vector list created by reading the values in the vectorFile */
template<class DataType>
std::map< std::string, std::shared_ptr<TvecInterface<DataType>> > loadSession()
{
	// Check for a saved session
	std::string fileName = "vectorFile";
	std::ifstream vectorFile(fileName, std::ios::in);
	if(!vectorFile)
	{
		throw("Previous session file not available.");
	}
	else
	{
		std::map<std::string, 
				 std::shared_ptr<TvecInterface<DataType>>> tempMap;
		std::string name; DataType x; DataType y;
		while(vectorFile >> name >> x >> y)
		{
			Point2D<DataType> p(x,y);
			std::shared_ptr<TvecInterface<DataType>> v = 
									std::make_shared< Tvec2D<DataType> >(p);
			tempMap.insert({name, v});
		}
		return tempMap;
	}
}

/** Display all the saved vectors for the session
@param vecList	 The map container that is holding all the vectors 
	for the user session. */
template<class DataType>
void showVectors(std::map<std::string, 
				 std::shared_ptr<TvecInterface<DataType>>>& vecList)
{
	// typename required to use map::iterator due to dependent scope
	typename std::map<std::string, 
					  std::shared_ptr<TvecInterface<DataType>>>::iterator it;
	std::cout << "\n\t---------------------------------" << std::endl;
	std::cout << "\tList of vectors: " << std::endl;
	for(it = vecList.begin(); it != vecList.end(); ++it)
	{
		std::cout << "\t" << std::setw(10) 
				  << it->first << " = " << it->second << std::endl;
	}
	std::cout << "\t---------------------------------\n" << std::endl;
}



/** Prompt user for relevant information and create a new vector. 
@return A dynamic pointer to a new vector	*/
template<class DataType>
std::shared_ptr<TvecInterface<DataType>> newVec()
{
	std::cout << "\n\t---------New vector options---------" << std::endl;
	std::cout << "\t1) Enter x,y coordinates" << std::endl;
	std::cout << "\t2) Enter magnitude and direction" << std::endl;
	std::cout << "\t3) Enter magnitude and bearing" << std::endl;
	std::cout << "\t------------------------------------" << std::endl;

	std::shared_ptr< TvecInterface<DataType> > temp;
	bool loop = true;
	while(loop)
	{
		std::cout << "Choose a method for creating the new vector: ";

		char selection;
		std::cin.get(selection);
		std::cin.clear(); 
		std::cin.ignore(1000, '\n');
		if( isalpha(selection) ){ selection = toupper(selection); }

		switch(selection)
		{
			case '1': 
			{
				DataType x; DataType y;
				std::cout << "Enter x and y coordinates: ";
				if( std::cin >> x >> y )
				{
					Point2D<DataType> p1(x,y);
					temp = std::make_shared< Tvec2D<DataType> >(p1); 
					loop = false;
				}
				else
				{
					std::cout << "Invalid Input. Please enter a decimal value." 
							  << std::endl;
					std::cin.clear(); 
					std::cin.ignore(1000, '\n');
				}
				break;
			}
			case '2': 
			{ 
				DataType mag; radians dir;
				std::cout << "Enter magnitude and direction(radians): ";
				if( std::cin >> mag >> dir)
				{
					temp = std::make_shared< Tvec2D<DataType> >(mag, dir);
					loop = false;
				}
				else
				{
					std::cout << "Invalid Input. Please enter a decimal value." 
							  << std::endl;
					std::cin.clear(); 
					std::cin.ignore(1000, '\n');
				}
				break;
			}
			case '3': 
			{  
				DataType mag; DataType bearing; radians dir;
				std::cout << "Enter magnitude and bearing: ";
				if( std::cin >> mag >> bearing )
				{
					bearing = std::fmod(bearing, 360.0);
					// Convert bearing to radian angle
					dir = 3.141592654*((360.0 - bearing) + 90.0) / 180.0;
					// Angle in radians 0 to 2Pi
					dir = std::fmod(dir, 2 * 3.141592654);
					std::cout << " bearing " << bearing 
							  << " = " << dir << " radians." << std::endl;
					temp = std::make_shared< Tvec2D<DataType> >(mag, dir);
					loop = false;
				}
				else
				{
					std::cout << "Invalid Input. Please enter a decimal value." 
							  << std::endl;
					std::cin.clear(); 
					std::cin.ignore(1000, '\n');
				}

				break;
			}
			default : 
			{  
				std::cout << "Not a valid option.Please try again." 
						  << std::endl;
				break;
			}
		}
	}
	
	return temp;
}


/** Prompt user to name and save the vector
@param vecList	The map container that is holding all the vectors 
	for the user session.
@param v1   The vector pointer to be saved
@param input   Flag to save or not save the vector to vector list. 
	Saving is the default flag on some calls.	  */
template<class DataType>
void savePrompt(std::map<std::string, 
				std::shared_ptr< TvecInterface<DataType>> >& vecList, 
				std::shared_ptr< TvecInterface<DataType> >& v1, 
				char input='n')
{
	if ( tolower(input) != 'y' )
	{
		std::cout << "Do you wish to save this vector(y/n)?";
		std::cin >> input;
	}
	std::cin.clear(); std::cin.ignore(1024, '\n');
	if(tolower(input) == 'y')
	{
		std::cout << "Enter a name for the vector(No spaces): ";
		std::string name;
		getline(std::cin, name);
		bool foundSpace = true;
		while(foundSpace)
		{
			foundSpace = false;
			for(int i = 0; i < name.size(); ++i)
			{
				if(name[i] == ' ')
				{
					foundSpace = true;
				}
			}
			if(foundSpace)
			{
				std::cout << "Error: The name contains a space." 
						  << std::endl;
				std::cout << "Enter a name for the vector(No spaces): ";
				getline(std::cin, name);
			}
		}

		typename std::map<std::string, 
					std::shared_ptr<TvecInterface<DataType>>>::iterator it;
		// map::find returns an iterator to end of map if item not found
		it = vecList.find(name); 
		// map does not allow duplicate names
		while( it != vecList.end() )
		{
			std::cout << "Vector name already exists. " 
					  << "Please try a new name : ";
			getline(std::cin, name);
			it = vecList.find(name); 
		}

		vecList.insert({name, v1});
		std::cout << "Saved vector :  " << name << " = " << v1 << std::endl;
	}
}

/** Remove a vector from the list holding the vectors for the session.
@param vecList	The map container that is holding all the vectors 
	for the user session */
template<class DataType>
void removeVec(std::map<std::string, 
			   std::shared_ptr< TvecInterface<DataType>> >& vecList)
{
	std::cout << "Enter the name of the vector to delete or "
			  << "enter 'delete all': ";
	std::string name;
	getline(std::cin, name);
	if(name == "delete all")
	{
		vecList.clear();
		std::cout << "Vector list cleared." << std::endl;
	}
	else
	{
		auto it = vecList.find(name); 
		if( it != vecList.end() )
		{
			std::cout << "Removing vector : " << name 
					  << " = " << vecList[name] << std::endl;
			// .erase returns  the number of vectors removed.
			int num = vecList.erase(name);
			std::cout << num << " vector succesfully removed." << std::endl;
		}
		else
		{
			std::cout << "The specified vector was not found." << std::endl;
		}
	}
}

/** Inform the user that there are not enough vectors to perform
	any computations. */
void vectorListIsEmpty()
{
	std::cout << "Not enough vectors in the vector list." << std::endl; 
	std::cout << "Select option N) from the menu to add a new vector." << std::endl;

}


/** Find a vector in the dictionary.
@param spec  The specifier that will appear on the prompt.
@param vecList	The map container that is holding all the vectors for 
	the user session.
@return  A dynamic pointer to a vector.	*/ 
template<class DataType>
std::shared_ptr<TvecInterface<DataType>> findVec(
			std::string spec,
			std::map<std::string, 
			std::shared_ptr<TvecInterface<DataType>>>& vecList)
{
	std::cout << "Enter the name of " << (spec == "" ? "a" : spec) 
			  << " vector or 'quit': ";
	std::string name;
	std::getline(std::cin, name);
	typename std::map<std::string, 
				std::shared_ptr< TvecInterface<DataType>> >::iterator it;
	// map::find returns an iterator to end of map if item not found
	it = vecList.find(name); 
	// Keep prompting until user quits or a valid vector is found
	while( name != "quit" && it == vecList.end() )
	{
		std::cout << "Not a valid vector name." << std::endl;
		std::cout << "Enter a new name or 'quit' to return to menu: ";
		std::getline(std::cin, name);
		it = vecList.find(name); 
	}
	if(name == "quit")
	{
		throw("Returning to menu.");
	}
	else
	{
		return it->second;
	}
}

/** Display information for a given vector
@param vec  The vector for which the info is displayed.  */
template<class DataType>
void showVecInfo(std::shared_ptr<TvecInterface<DataType>>& vec)
{
	std::cout << "\n\t-----------Vector Information -------------" << std::endl;
	std::cout << "\tMagnitude = " << vec->magnitude() << std::endl;
	if ( vec->magnitude() != 0 )
	{
		auto dir = vec->direction();
		std::cout << "\tDirection = " << dir << " radians" << std::endl;

		dir = (180.0*dir)/3.141592654; 
		std::cout << "\t          = " << dir << " degrees" << std::endl;
		dir = (360.0 - dir) + 90;
		dir = std::fmod(dir, 360.0); 
		std::cout << "\tBearing   = " <<  dir << " degrees due North" << std::endl;
		std::cout << "\t-------------------------------------------\n" << std::endl;
	}
}


/** Prompt user for a scalar value.
@return  The scalar value
This function allows the scalar variable in main to be templatized  */
template<class DataType>
DataType getScalar()
{
	std::cout << "Enter the scalar value: ";
	DataType scalar; 
	while( ! (std::cin >> scalar) )
	{
		std::cin.clear(); std::cin.ignore(1024, '\n');
		std::cout << "Invalid input. Please enter a valid decimal: ";
	}
	return scalar;
}


/** Save the vector list to a file for future sessions.
@param vecList   The map data structure for storing vectors using 
	their corresponding user created names */
template<class DataType>
void saveSession(std::map<std::string, 
				 std::shared_ptr<TvecInterface<DataType>>>& vecList)
{
	// Check for a saved session
	std::string fileName = "vectorFile";
	std::ofstream vectorFile(fileName, std::ios::out);
	
	if(!vectorFile)
	{
		std::cerr << "File " << fileName << " found." << std::endl;
	}
	else
	{
		for(auto i : vecList)
		{
			vectorFile << i.first << "\t"
				<< std::dynamic_pointer_cast<Tvec2D<DataType>>(i.second)->x()
				<< "\t"
				<< std::dynamic_pointer_cast<Tvec2D<DataType>>(i.second)->y()
				<< "\n";
		}
	}
	std::cout << "Saved vector list to file '" << fileName << "'" << std::endl;
}





#endif
