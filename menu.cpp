/** @file menu.h
 	@author: Lucas Anesti */
/*	Description: The menu functions used by the main driver  */

#ifndef MENU
#define MENU

#include <iostream>
#include <fstream>
#include <iomanip> 	// std::setw
#include <string>
#include <memory>	// std::shared_ptr
#include <map>		// std::map
#include <iterator>  // std::iterator

#include "Point2D.h"
#include "Tvec2D.h"
#include "menuFunctions.cpp"




int menu()
{
	// Storing the vectors that were generated in the last session in a map
	// (dictionary) data structure, with user defined names serving as 
	// keys.
	std::map< std::string, std::shared_ptr< TvecInterface<double> >> vectorList;

	// The vectors saved into a file from the previous sessions are loaded 
	// into vectorList. If one does not exists, user is notified.
	try
	{
		vectorList = loadSession<double>();
	}
	catch(const char* msg)
	{
		std::cerr << msg << std::endl;
		std::cout << "A save file will be created upon exiting the "
				  << "next session." << std::endl;
	}


	bool loop = true;
	while(loop)
	{
		displayMenu();

		char selection;
		std::cin.get(selection);
		std::cin.clear(); 
		std::cin.ignore(1000, '\n');
		if( isalpha(selection) ){ selection = toupper(selection); }

		switch(selection)
		{
			// Show the saved vectors
			case 'L': 
			{
				showVectors(vectorList); break;
			}
			// Create and store a new vector
			case 'N': 
			{
				auto v1 = newVec<double>();
				std::cout << "Created vector: " << v1 << std::endl;
				savePrompt(vectorList, v1, 'y');
				break;
			}
			// Delete a vector
			case 'D': 
			{
				removeVec(vectorList); break;
			}
			// Solve Ground/Air speed problems
			case 'G': 
			{
				std::cout << "\nSolve vector problems involving air, "
						  << "ground, and wind speed."
						  << std::endl;
				bool ask = true;

				if( vectorList.size()<2 )
				{
					vectorListIsEmpty();
					ask=false;
				}

				if(ask)
				{
					std::cout << "\n\t-----------------" << std::endl;
					std::cout << "\tA) Air speed\n"
							  << "\tG) Ground speed\n" 
							  << "\tW) Wind speed\n" 
							  << "\tQ) Main Menu" << std::endl;
					std::cout << "\t-----------------" << std::endl;
					std::cout << "Solving for ? ";
					char choice;
					std::cin.get(choice);
					std::cin.clear(); 
					std::cin.ignore(1000, '\n');
					if( isalpha(choice) ){ choice = toupper(choice); }
					
					switch(choice)
					{
						case 'A':  
						{
							try
							{
								auto ground = findVec("the ground", vectorList);
								auto wind = findVec("the wind", vectorList);

								auto air = ground->sub(wind);

								showVecInfo(air);
								savePrompt(vectorList, air);
							}
							catch(const char* msg)
							{
								std::cerr << msg << std::endl;
							}
							break;
						}
						case 'G':  
						{
							try
							{
								auto air = findVec("the air", vectorList);
								auto wind = findVec("the wind", vectorList);

								auto ground = air->add(wind);

								showVecInfo(ground);
								savePrompt(vectorList, ground);
							}
							catch(const char* msg)
							{
								std::cerr << msg << std::endl;
							}
							break;
						}
						case 'W': 
						{
							try
							{
								auto ground = findVec("the ground", vectorList);
								auto air = findVec("the air", vectorList);

								auto wind = ground->sub(air);

								showVecInfo(wind);
								savePrompt(vectorList, wind);
							}
							catch(const char* msg)
							{
								std::cerr << msg << std::endl;
							}
							break;
						}
						case 'Q': { ask = false; break; }
						default : 
						{ 
							std::cout << "Not a valid entry. Please try again." 
									  << std::endl;
							break;
						}
					}
				}
				break;
			}
			// Add two vectors and store the result if 'y'
			case '1': 
			{
				if( vectorList.size() > 1 )
				{
					try
					{
						auto v1 = findVec("the first", vectorList);
						auto v2 = findVec("the second", vectorList);
						auto v3 = v1->add(v2);
						std::cout << "Result = " << v3 << std::endl;
						savePrompt(vectorList, v3);
					}
					catch(const char* msg)
					{
						std::cerr << msg << std::endl;
					}
				}
				else
				{
					vectorListIsEmpty();
				}
				break;
			}
			// Subtract 2 vectors and store result if 'y'
			case '2': 
			{
				if( vectorList.size() > 1 )
				{
					try
					{
						auto v1 = findVec("the first", vectorList);
						auto v2 = findVec("the second", vectorList);
						auto v3 = v1->sub(v2);
						std::cout << "Result = " << v3 << std::endl;
						savePrompt(vectorList, v3);
					}
					catch(const char* msg)
					{
						std::cerr << msg << std::endl;
					}
				}
				else
				{
					vectorListIsEmpty();
				}
				break;
			}
			// Multiply a vector by a scalar
			case '3': 
			{
				if( vectorList.size() > 0 )
				{
					try
					{
						auto v1 = findVec("", vectorList);
						auto v2 = v1->mul(getScalar<double>());
						std::cout << "Result = " << v2 << std::endl;
						savePrompt(vectorList, v2);
					}
					catch(const char* msg)
					{
						std::cerr << msg << std::endl;
					}
				}
				else
				{
					vectorListIsEmpty();
				}
				break;
			}
			// Divide a vector by a scalar
			case '4': 
			{
				if( vectorList.size() > 0 )
				{
					try
					{
						auto v1 = findVec("", vectorList);
						double scalar = getScalar<double>();
						while( scalar == 0)
						{
							std::cout << "Division by zero not allowed. ";
							std::cin.clear(); std::cin.ignore(1000, '\n');
							scalar = getScalar<double>();
						}
						auto v2 = v1->div( scalar );
						std::cout << "Result = " << v2 << std::endl;
						savePrompt(vectorList, v2);
					}
					catch(const char* msg)
					{
						std::cerr << msg << std::endl;
					}
				}
				else
				{
					vectorListIsEmpty();
				} 
				break;
			}
			// Get direction
			case '5': 
			{
				if( vectorList.size() > 0 )
				{
					try
					{
						auto v1 = findVec("", vectorList);
						showVecInfo(v1);
					}
					catch(const char* msg)
					{
						std::cerr << msg << std::endl;
					}
				}
				else
				{
					vectorListIsEmpty();
				}
				break;
			}
			// Dot
			case '6': 
			{
				if( vectorList.size() > 1 )
				{
					try
					{
						auto v1 = findVec("the first", vectorList);
						auto v2 = findVec("the second", vectorList);
						std::cout << "Dot product = " << v1->dot(v2) << std::endl;
					}
					catch(const char* msg)
					{
						std::cerr << msg << std::endl;
					}
				}
				else
				{
					vectorListIsEmpty();
				}
				break;
			}
			// Unit vector
			case '7': 
			{
				if( vectorList.size()>0 )
				{
					try{
						auto v1 = findVec("the first", vectorList);
						auto v2 = v1->unit();
						std::cout << "Unit vector = " << v2 << std::endl;
						savePrompt(vectorList, v2);
					}
					catch(const char* msg)
					{
						std::cerr << msg << std::endl;
					}
				}
				else
				{
					vectorListIsEmpty();
				}
				break;
			}

			case 'Q': 
			{
				loop = false; 
				std::cout << "Exiting program." << std::endl; 
				break;
			}

			default : 
			{
				std::cout << "\n***Not a valid menu option. "
						  << "Please try again.\n" << std::endl;
				break;
			}			
		}
	}

	saveSession(vectorList);

	return EXIT_SUCCESS;
}


#endif
