/** @file Point2D.h
 	@author: Lucas Anesti  */
/*	Description: a class to represent points on the x,y coordinate system  */

#ifndef POINTH
#define POINTH

#include<iostream>	// std::cout, std::endl
#include<cmath>		// pow, sqrt


//a class for x and y coordinates on the Cartesian plane
template<class DataType>
class Point2D
{
private:

	DataType x;
	DataType y;

public:

	// Constructors

	/** Constructor sets point to (a,b).
		If no parameters are supplied, the point is set to (0,0) and
		operates as a default constructor */
	Point2D(const DataType& a = 0, const DataType& b = 0);



	// Getters

	/** Get the x coordinate of the point.
	@return the value of the x coordinate. */
	DataType getx() const;

	/** Get the y coordinate of the point.
	@return the value of the y coordinate. */
	DataType gety() const;
	


	// Setters

	/** Set the x and y values of the point.
	@param newX  A reference to the new value for the x coordinate.
	@param newY  A reference to the new value for the y coordinate. */
	void set(const DataType& newX, const DataType& newY);



	// Other methods

	/** Get the distance between this point and the origin
	@return The value of the distance between this point and the origin
		expressed as DataType. */
	DataType dis() const;

	/** Get the distance between this point and another 2D point.
	@param other  A reference to a Point2D object, for which the distance 
		from the current object we want to calculate.
	@return The value of the distance between this point and another
		point expressed as DataType. */
	DataType dis(const Point2D<DataType>& other) const;

	/** Midpoint between point and origin
	@return A new point that represents the midpoint between this
		point and the origin. */
	Point2D<DataType> mid() const;

	/** Midpoint between this point an another 2D point.
	@param other  A reference to a Point2D object, for which the midpoint 
		from the current object we want to calculate.
	@return A new point that represents the midpoint between the two points. */	
	Point2D<DataType> mid(const Point2D<DataType>& other) const;

};


/** Overloaded operator<<
@param p  Reference to a Point2D object.
@param o  Reference to an output stream.
@return A reference to the output stream.	*/
template<class DataType>
std::ostream& operator<<(std::ostream& o, const Point2D<DataType>& p);



#include"Point2D.cpp"

#endif




