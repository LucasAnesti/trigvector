/**	@file test.cpp
	@author  Lucas Anesti	*/

#include <cstdlib>		// EXIT_SUCCESS
#include <iostream>
#include <iomanip>		// std::setw
#include <cmath>			// abs
#include <string>
#include <stdexcept>	// std::logic_error

#include "Point2D.h"
#include "Tvec2D.h"
#include "testFunctions.cpp"



void testPoint()
{
	double result; double result1; double result2;


	displayHeader("Point2D Constructors");

	Point2D<double> p1;
	displayResult("Default Constr: x", p1.getx(), 0.0);
	displayResult("Default Constr: y", p1.gety(), 0.0);

	double x = 5.67; double y = 6.21;
	Point2D<double> p2(x,y);
	displayResult("Constr(x,y): x", p2.getx(), x);
	displayResult("Constr(x,y): y", p2.gety(), y);


	displayHeader("Point2D Setters/Getters");

	x = -6.4; y = 12.6;
	p2.set(x,y);
	displayResult("Point2D.set(x,y): x", p2.getx(), x);
	displayResult("Point2D.set(x,y): y", p2.gety(), y);
	

	displayHeader("Point2D Methods");

	//distance to origin
	result = sqrt( pow(x,2) + pow(y,2) );
	displayResult("Point2D.dis()", p2.dis() ,result);

	//distance to another point
	Point2D<double> p2b(10, 10);
	result = sqrt( pow(10-x,2) + pow(10-y,2) );
	displayResult("Point2D.dis(point)", p2.dis(p2b) ,result);

	//mid point to origin
	Point2D<double> p3 = p2.mid();
	displayResult("Point2D.mid(): x", p3.getx(), p2.getx()/2.0);
	displayResult("Point2D.mid(): y", p3.gety(), p2.gety()/2.0);

	//mid point from p3 to p2
	Point2D<double> p3b = p3.mid(p2);
	result1 = ( p2.getx() + p3.getx() )/2.0;
	result2 = ( p2.gety() + p3.gety() )/2.0;
	displayResult("Point2D.mid(point): x", p3b.getx(), result1);
	displayResult("Point2D.mid(point): y", p3b.gety(), result2);

	//overloaded << 
	std::cout << std::setw(NAME_WIDTH) << "cout << Point2D"
		  	  << std::setw(COLUMN_WIDTH - 9)  << p3
			  << std::setw(COLUMN_WIDTH - 9)  
			  << "("<<p3.getx()<<","<<p3.gety()<<")" 
			  << std::setw(RESULT_WIDTH) << " - " 
	 		  << std::endl;

}


void testVector()
{
	double result; double result1; double result2;

	displayHeader("Tvec2D Constructors");

	Tvec2D<double> vec1;
	displayResult("Def. Constr.: x", vec1.x(), 0.0);
	displayResult("Def. Constr.: y", vec1.y(), 0.0);

	Point2D<double> p2(4,6);
	Tvec2D<double> vec2(p2);
	displayResult("Constr(point): x", vec2.x(), p2.getx() );
	displayResult("Constr(point): y", vec2.y(), p2.gety() );

	Point2D<double> p3(-20, -20);
	Tvec2D<double> vec3(p2, p3);
	result1 = p3.getx() - p2.getx(); 
	result2 = p3.gety() - p2.gety(); 
	displayResult("Constr(p1, p2): x", vec3.x(), result1);
	displayResult("Constr(p1, p2): y", vec3.y(), result2);

	Tvec2D<double> vec4(2, Pi/6);
	displayResult("Constr(mag, dir): x", vec4.x(), sqrt(3));
	displayResult("Constr(mag, dir): y", vec4.y(), 1.0); 


	displayHeader("Tvec2D Setters/Getters");

	// set()
	vec4.set(200.0,100.0);
	displayResult("Tvec2D.set(x,y): x", vec4.x(), 200.0);
	displayResult("Tvec2D.set(x,y): y", vec4.y(), 100.0); 



	displayHeader("Tvec2D Methods");


	// mag()
	Point2D<double> p5(-20,-20);
	Tvec2D<double> vec5(p5);
	result1 = vec5.magnitude();
	result2 = p5.dis();
	displayResult("magnitude():", result1, result2);


	// dir()
	Point2D<double> pq1(5,5);   Tvec2D<double> vq1(pq1);
	Point2D<double> pq2(-5,5);  Tvec2D<double> vq2(pq2);
	Point2D<double> pq3(-5,-5); Tvec2D<double> vq3(pq3);
	Point2D<double> pq4(5,-5);  Tvec2D<double> vq4(pq4);
	Point2D<double> pv1(0,5);   Tvec2D<double> vv1(pv1);
	Point2D<double> pv2(0,-5);  Tvec2D<double> vv2(pv2);
	Point2D<double> ph1(5,0);   Tvec2D<double> vh1(ph1);
	Point2D<double> ph2(-5,0);  Tvec2D<double> vh2(ph2);
	Point2D<double> p0_0(0,0);  Tvec2D<double> v0_0(p0_0);

	try
	{
		displayResult("direction():  45", vq1.direction(), Pi/4);
		displayResult("direction(): 135", vq2.direction(), 3*Pi/4);
		displayResult("direction(): 225", vq3.direction(), 5*Pi/4);
		displayResult("direction(): 315", vq4.direction(), 7*Pi/4);
		displayResult("direction():  90", vv1.direction(), Pi/2);
		displayResult("direction(): 270", vv2.direction(), 3*Pi/2);
		displayResult("direction():   0", vh1.direction(), 0.0);
		displayResult("direction(): 180", vh2.direction(), Pi);

		// 0 magnitude shouls throw an exception
		v0_0.direction();
	}
	catch(std::logic_error e)
	{
		std::cerr << std::setw(NAME_WIDTH) << "direction(): 0 MAG"
		 		  << std::setw(COLUMN_WIDTH) << e.what()
				  << std::setw(COLUMN_WIDTH) << "Error: mag = 0" 
		  		  << std::setw(RESULT_WIDTH) << " - " << std::endl;
	}



	displayHeader("Methods from TvecInterface");


	// dot()
	Point2D<double> p6(5,5); 
	Point2D<double> p7(-5,-5);
	std::shared_ptr< TvecInterface<double> > vec6 = 
									std::make_shared<Tvec2D<double>>(p6); 
	std::shared_ptr< TvecInterface<double> > vec7 = 
									std::make_shared<Tvec2D<double>>(p7); 
	result1 = vec6->dot(vec7);
	result2 = -50;
	displayResult("dot()", result1, result2);


	// angle()
	Point2D<double> p8(1,0); 		
	std::shared_ptr< TvecInterface<double> > vec8 = 
									std::make_shared< Tvec2D<double> >(p8); 
	std::shared_ptr< TvecInterface<double> > vec9a;
	std::shared_ptr< TvecInterface<double> > vec9b;
	std:: cout << " 	  ------------ angles: increment by Pi/4 -----------" 
			   << std::endl;
	for(int i = 0; i <= 8; ++i)
	{
		vec9a = std::make_shared< Tvec2D<double> >(1, i*(Pi/4));
		result1 = vec8->angle(vec9a);
		// Absolute value function to generate expected value.
		result2 = -fabs( (Pi/4)*(i-4) ) + Pi;
		// Convert angle to string for display.
		std::string angle = std::to_string(i)+"Pi/4"; 
		displayResult("angle(): " + angle, result1, result2);
	}
	std::cout << " 	  ------------ angles: increment by Pi/6 -----------" 
			  << std::endl;
	for(int i = 0; i <= 12; ++i)
	{
		vec9a = std::make_shared< Tvec2D<double> >(1, i*(Pi/6));
		result1 = vec8->angle(vec9a);
		// Absolute value function to generate expected values
		result2 = -fabs( (Pi/6)*(i-6) ) + Pi;
		// Convert angle to string for display.
		std::string angle = std::to_string(i)+"Pi/6"; 
		displayResult("angle(): " + angle, result1, result2);
	}
	try
	{
		// Zero magnitude should throw an exception
		vec9a = std::make_shared< Tvec2D<double> >();
		result1 = vec8->angle(vec9a);
	}
	catch(std::logic_error e)
	{
		std::cerr << std::setw(NAME_WIDTH) << "angle(): 0 MAG"
		  	  	  << std::setw(COLUMN_WIDTH) << e.what()
			  	  << std::setw(COLUMN_WIDTH) << "Error: mag = 0" 
		  	  	  << std::setw(RESULT_WIDTH) << " - " << std::endl;
	}
	std::cout << " 	  ---------------------------------------------------"
			  << std::endl;


	// isEqual()
	Point2D<double> p10(5,5); 	Point2D<double> p11(-5,5); 
	std::shared_ptr< TvecInterface<double> > vec10a = 
									std::make_shared<Tvec2D<double>>(p10);
	std::shared_ptr< TvecInterface<double> > vec10b = 
									std::make_shared<Tvec2D<double>>(p10);
	std::shared_ptr< TvecInterface<double> > vec11 = 
									std::make_shared<Tvec2D<double>>(p11);
	result1 = vec10a->isEqual(vec10b);
	displayResult("isEqual()", result1, 1.0);
	result1 = vec10a->isEqual(vec11);
	displayResult("isEqual()", result1, 0.0);


	// isOrtho()
	result1 = vec10a->isOrtho(vec10b);
	displayResult("isOrtho()", result1, 0.0);
	result1 = vec10a->isOrtho(vec11);
	displayResult("isOrtho()", result1, 1.0);


	// add()
	Point2D<double> p12(5,2); 	Point2D<double> p13(3,10);
	std::shared_ptr<TvecInterface<double>> v12 = 
								std::make_shared<Tvec2D<double>>(p12);
	std::shared_ptr<TvecInterface<double>> v13 = 
								std::make_shared<Tvec2D<double>>(p13);
	auto v14 = v12->add(v13);
	// Downcasting to get the x and y coordinates
	std::shared_ptr<Tvec2D<double>> temp14 = 
						std::dynamic_pointer_cast< Tvec2D<double> >(v14);
	result1 = temp14->x();
	result2 = temp14->y();
	displayResult("add(): x", result1, 8.0);
	displayResult("add(): y", result2, 12.0); 


	// mult()
	double scalar15 = 2.5;
	auto v15 = v14->mul(scalar15);
	std::shared_ptr<Tvec2D<double>> temp15 = 
					std::dynamic_pointer_cast<Tvec2D<double>>(v15);
	result1 = temp15->x();
	result2 = temp15->y();
	displayResult("mul(): x", result1, 8 * scalar15);
	displayResult("mul(): y", result2, 12 * scalar15); 


	// sub()
	Point2D<double> p16(5,10); 	Point2D<double> p17(4,8);
	std::shared_ptr<TvecInterface<double>> v16 = 
							std::make_shared<Tvec2D<double>>(p16);
	std::shared_ptr<TvecInterface<double>> v17 = 
							std::make_shared<Tvec2D<double>>(p17);
	auto v18 = v16->sub(v17);
	std::shared_ptr<Tvec2D<double>> temp18 = 
					std::dynamic_pointer_cast<Tvec2D<double>>(v18);
	result1 = temp18->x();
	result2 = temp18->y();
	displayResult("sub(), x", result1, 1.0);
	displayResult("sub(), y", result2, 2.0); 


	// div()
	auto v19 = v17->div(2);
	std::shared_ptr<Tvec2D<double>> temp19 = 
					std::dynamic_pointer_cast<Tvec2D<double>>(v19);
	result1 = temp19->x();
	result2 = temp19->y();
	displayResult("div(): x", result1, 2.0);
	displayResult("div(): y", result2, 4.0); 

	// Division by 0 should throw an exception
	try
	{
		auto v20 = v17->div(0);
	}
	catch(std::logic_error e)
	{
		std::cerr << std::setw(NAME_WIDTH) << "div(0)"
				  << std::setw(COLUMN_WIDTH) << e.what()
				  << std::setw(COLUMN_WIDTH) << "Error: Div by 0" 
		  		  << std::setw(RESULT_WIDTH) << " - " << std::endl;
	}

	// print()
	std::shared_ptr<TvecInterface<double>> v_print = 
									std::make_shared<Tvec2D<double>>();
	std::cout << std::setw(NAME_WIDTH) << "cout << Tvec2D"
		 	  << std::setw(COLUMN_WIDTH-4)  << v_print //Compensate for ostream
	 	 	  << std::setw(COLUMN_WIDTH)  << "<0,0>" 
			  << std::setw(RESULT_WIDTH) << " - " 
	 		  << std::endl;

}




int main()
{
	std::cout << "\n";

	testPoint();
	testVector();

	std::cout << "\n";

	return EXIT_SUCCESS;
}
