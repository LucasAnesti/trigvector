/**	@file TvenInterface.h
	@author  Lucas Anesti	*/

/*	Description: An abstract class designed to provide the specifications 
	for derived trigonometric vector classes.  */

#ifndef TvecInterface_
#define TvecInterface_

#include<memory> 	//std::shared_ptr

/* 	Both radians and DataType represent real numbers. 
	Declared as an alias for clarity */
#define radians DataType
#define Pi 3.14159265359

template<class DataType>
class TvecInterface
{
public:
	
	/** Get the magnitude of the vector.
	@return magnitude |v| of vector v expressed as DataType.
		The magnitude is calculated as the distance of the point
		from the origin. */
  	virtual DataType magnitude() const = 0;

	/**Get direction of the vector in radians
	@pre The vector must have a non-zero magnitude
	@post Radian measure always within 0 and 2pi
		  Throw an exception if precondition not met
	@return  Radian (alias for double) measure of angle of vector
			 with respect to  positive x-axis. */
	virtual radians direction() const = 0 ;

	/** Get the unit vector in the same direction as this vector.
	@pre The vector must have a non-zero magnitude.
	@return	Pointer to a unit vector in the same direction as
		the original vector. 		*/ 
  	virtual std::shared_ptr<TvecInterface<DataType>> unit() const = 0;

	/** Get the dot product of this and another vector
	@pre Parameter 'other' must call only member functions inherited 
	  	 by TvecInterface for polymorphism to work.
	  	 Vectors must be of the same DataType. 
	@param other  Reference to a dynamic base pointer to an object of 
		 the derived class type.
	@return The dot product of two vectors expressed as double. */
	virtual DataType dot(
		const std::shared_ptr<TvecInterface<DataType>>& other) const = 0;

	/** Get angle between this and another vector
	@pre Parameter 'other' must call only member functions inherited 
	  	 by TvecInterface for polymorphism to work.
	  	 Vectors must be of the same DataType.
		 Both vectors must have a non-zero magnitude.
	@param other  Reference to a dynamic base pointer to an object of 
		 the derived class type.
	@return  Radian (alias for double) measure of angle between 
			 two vectors. Angle is always between 0 and Pi radians. */
	virtual radians angle(
		const std::shared_ptr<TvecInterface<DataType>>& other) const = 0;
	
	/** Test if this vector is equal to another one
	@pre Parameter 'other' must call only member functions inherited 
	  	 by TvecInterface for polymorphism to work.
	  	 Vectors must be of the same DataType.
		 Both vectors must have a non-zero magnitude.
	@param other  Reference to a dynamic base pointer to an object of 
		 the derived class type.
	@return True if the vectors have equal magnitude and direction. */
	virtual bool isEqual(
		const std::shared_ptr<TvecInterface<DataType>>& other) const = 0;
	
	/** Test if this vector is orthogonal to another vector
	@pre Parameter 'other' must call only member functions inherited 
	  	 by TvecInterface for polymorphism to work.
	  	 Vectors must be of the same DataType.
	@param other  Reference to a dynamic base pointer to an object of 
		 the derived class type.
	@return True if the vectors are orthogonal. */
	virtual bool isOrtho(
		const std::shared_ptr<TvecInterface<DataType>>& other) const =0;

	/** Add this vector and another vector
	@pre Parameter 'other' must call only member functions inherited 
	  	 by TvecInterface for polymorphism to work.
	  	 Vectors must be of the same DataType.
	@param other  Reference to a dynamic base pointer to an object of 
		 the derived class type.
	@return Pointer to a new derived object, result of adding one 
			vector to another.*/ 
 	virtual std::shared_ptr<TvecInterface<DataType>>
		 add(const std::shared_ptr<TvecInterface<DataType>>& vec) const = 0;

	/** Subtract another vector from this vector
	@pre Parameter 'other' must call only member functions inherited 
	  	 by TvecInterface for polymorphism to work.
	  	 Vectors must be of the same DataType.
		 The second vectors is subtracted from the first.
	@param other  Reference to a dynamic base pointer to an object of 
		 the derived class type.
	@return Pointer to a new derived object, result of subtracting one 
			vector from another.*/ 
 	virtual std::shared_ptr<TvecInterface<DataType>> 
		sub(const std::shared_ptr<TvecInterface<DataType>>& vec) const = 0;

	/** Multiply a vector by a scalar variable
	@pre Scalar multiplication is not commutative and must be called as a 
	  	 member function of the vector. 
	@return Pointer to a new derived object, result of multiplying
			a vector by a scalar. */
	virtual std::shared_ptr<TvecInterface<DataType>> mul(
										const DataType& scalar) const =0;

	/** Divide a vector by a scalar variable
	@pre Scalar division is not commutative and must be called as a
	  	 member function of the vector. 
		 Throws an exception if division by zero is attempted.
	@return Pointer to a new derived object, result of dividing
			a vector by a scalar. */
	virtual std::shared_ptr<TvecInterface<DataType>> 
									div(const DataType& scalar) const =0;

	/** Display the vector in <x,y [,z,...]> format			*/
	virtual std::ostream& print(std::ostream&) const = 0; 
	
};


/** Overloaded operator<<
@param vec  Reference to a dynamic base pointer to an object of the 
	derived class type.
@param os  Reference to an output stream.
@pre vec must be a polymorphic pointer of type TvecInterface.
@return A reference to the output stream.	*/
template<class DataType>
std::ostream& operator<<(std::ostream& os, 
						 const std::shared_ptr<TvecInterface<DataType>>& vec)
{
	return vec->print(os);	
}



#endif
