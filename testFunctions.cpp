/**	@file testFunctions.cpp
	@author  Lucas Anesti	*/

/*  Description: Generic functions called by the test.cpp file*/

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>	// fabs


const int NAME_WIDTH = 25;
const int COLUMN_WIDTH = 18;
const int RESULT_WIDTH = 15;
const int TITLE_WIDTH = NAME_WIDTH + 2 * COLUMN_WIDTH + RESULT_WIDTH;

/**	Formatted display of output headers and test results
@param name  Indicates the category of methods being testes */
void displayHeader(std::string name){

	std::string title = "TESTING: ";

	int width = TITLE_WIDTH - name.length() - title.length();

	std::cout << "\n" << std::string(width/2, '-') << title << name 
		 	  << std::string(width/2, '-') << std::endl;

	std::cout << std::setw(NAME_WIDTH) << "Method"
			  << std::setw(COLUMN_WIDTH) << "Output" 
		 	  << std::setw(COLUMN_WIDTH) << "Expected" 
			  << std::setw(RESULT_WIDTH) << "Outcome" 
		 	  << std::endl;

	std::cout << std::setw(NAME_WIDTH) << "======"
			  << std::setw(COLUMN_WIDTH) << "======" 
		 	  << std::setw(COLUMN_WIDTH) << "========" 
			  << std::setw(RESULT_WIDTH) << "=======" 
		 	  << std::endl;
}

/**	Compares actual and expected outputs and displays the result
	of the comparison. display of output headers and test results
@param name  Indicates the name of the method being testes
@param output  The actual output of the method
@expected expected  The expected output of the method being tested */
template<class DataType>
void displayResult(std::string name, DataType output, DataType expected){

	// Answer is correct if within 0.00001 of expected result
	bool result = fabs(output - expected) < 0.00001;

	std::cout << std::setw(NAME_WIDTH) << name
		  	  << std::setw(COLUMN_WIDTH) << output
	 	  	  << std::setw(COLUMN_WIDTH) << expected
		  	  << std::setw(RESULT_WIDTH) << (result? "PASS":"* FAIL")
	 	  	  << std::endl;
}


