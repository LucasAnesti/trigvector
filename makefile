# simple makefile for the Tvec project.  

FILES= Point2D.cpp Tvec2D.cpp main.cpp 
TESTFILES = Point2D.cpp Tvec2D.cpp test.cpp 
TARGET = Tvec.run
CC = g++
C11 = -std=c++11

#make  : just the executable
simple:
	$(CC) $(C11) $(FILES) -o $(TARGET)
	
#make run  : issue command 'make run' to run the executable file
run:
	./$(TARGET)
#make test	: run the test.cpp file
test:
	$(CC) $(C11) $(TESTFILES) -o test.run
	./test.run
	rm test.run

#make clean : removes all but source files
clean:
	rm -f *.o *.s $(TARGET) gmon.out *.gprof *.objdump
