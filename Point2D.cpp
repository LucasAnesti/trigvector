/** @file Point2D.cpp
	@author: Lucas Anesti */
/*	Description: Implementation file for the Point2D class  */

#ifndef POINTCPP
#define POINTCPP

#include"Point2D.h"

// Constructors

template<class DataType>
Point2D<DataType>::Point2D(const DataType& a, const DataType& b): x(a), y(b)
{}


// Getters

template<class DataType>
DataType Point2D<DataType>::getx() const{ return x; }

template<class DataType>
DataType Point2D<DataType>::gety() const{ return y; }


// Setters

template<class DataType>
void Point2D<DataType>::set(const DataType& newX, const DataType& newY)
{
	x = newX; 
	y = newY;
}



// Other methods

template<class DataType>
DataType Point2D<DataType>::dis() const
{
	return sqrt( pow(x,2) + pow(y,2) );
}

template<class DataType>
DataType Point2D<DataType>::dis(const Point2D<DataType>& other) const
{
	return sqrt( pow((other.x - x),2) + pow((other.y - y),2) );
}

template<class DataType>
Point2D<DataType> Point2D<DataType>::mid() const
{
	return 	Point2D( x/2.0, y/2.0 );
}

template<class DataType>
Point2D<DataType> Point2D<DataType>::mid(const Point2D<DataType>& other) const
{
	return 	Point2D( (x + other.x)/2.0, (y + other.y)/2.0 );
}

template<class DataType>
std::ostream& operator<<(std::ostream& o, const Point2D<DataType>& p)
{
	o << "(" << p.getx() << "," << p.gety() << ")";
	return o;
}


#endif
